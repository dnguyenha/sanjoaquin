***Author: Nguyen Ha Dao
Student number: 8695356
Email: daonguyenha5356@conestogac.on.ca

*** Installation Instruction
1. Create a new folder for the project, go to that folder.
2. Download/ Clone the repository to the local machine with command:
git clone https://dnguyenha@bitbucket.org/dnguyenha/sanjoaquin.git .
3. Open the website by double click on 'index.html', Firefox or Chrome brower is recommended.

* Note: This project is licensed under the 'MIT License' which is - 
A short and simple permissive license with conditions only requiring preservation of copyright and license notices. 
Licensed works, modifications, and larger works may be distributed under different terms and without source code.